const Twitter = require('twitter');
const config = require('./config');
const { mk_error_response, mk_ok_response } = require('./utils');
/*
library doc - https://www.npmjs.com/package/twitter
twitter API doc - https://developer.twitter.com/en/docs/api-reference-index
twitter API - tweet object - https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
twitter API - user object - https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object


Obtener user credentials:
  1. Loggearse a twitter
  2. IR a https://apps.twitter.com/
  3. Crear una app
     - en website poner http://www.example.com
  4. Ir a Keys and
  5. Poner las credenciales en el archivo config.js
*/

const client = new Twitter(config.auth.twitter);

function userWithMoreFollowers(tweets) {
  //Esta función me devuelve el usuario con más followers
  
  let maxUser = null;
  let maxFollowers = -1;

  tweets.forEach((tweet) => {
    
    if(tweet.user.followers_count > maxFollowers) {
      maxFollowers = tweet.user.followers_count;
      maxUser = tweet.user;
    }
  });
  return maxUser;
}

function returnTweetsTheUser(tweets) {
  
  const tweet1 = {};
  const tweet2 = {};

  tweet1.text = tweets[0].text;
  tweet1.author = tweets[0].user.screen_name;

  tweet2.text = tweets[1].text;
  tweet2.author = tweets[1].user.screen_name;

  const dataResponse = mk_ok_response([tweet1, tweet2]);
  
  return dataResponse;
}


function tweets(req, res) {
  const city = JSON.parse(req.query.city);
  const cityName = city.name;
  
  const paramsTweets = {
    q: cityName,
    count: 10,
  };

  client.get('search/tweets', paramsTweets).then(
    (response) => {
      const tweets = response.statuses;
      return tweets;
    
    }).then((tweets) => {
    
    const maxUser = userWithMoreFollowers(tweets);
    return maxUser;

  }).then((user) => {

    const paramsResponse = {
      screen_name: user.screen_name,
      count: 2,
    };

    const respuesta = client.get('statuses/user_timeline', paramsResponse);
    return respuesta;

  }).then((tweets) => {

    const dataResponse = returnTweetsTheUser(tweets);
    return res.json(dataResponse);
      
  }).catch((error) => {
        
    res.json(mk_error_response(error)); 
    
  });
}

module.exports = tweets;
