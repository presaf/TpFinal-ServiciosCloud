const config = require('./config');
const { mk_error_response, mk_ok_response } = require('./utils');

const GooglePlaces = require( 'node-googleplaces' );
const client = new GooglePlaces(config.auth.googleplaces);
//const Itinerary = require('./itineraryClass');

function places(req, res) {

  const city = JSON.parse(req.query.city);
  const cityId = city.place_id;

  let itinerary= new Itinerary();
  const cantidadDiasIt=2;

  client.details(
    { place_id: cityId }).then((response) => {
    console.log(response.body);
    const location = response.body.result.geometry.location;
    return location;

  }).then((location) => {

    var paramsResponse = {
      location:`${location.lat},${location.lng}`,
      radius:2000
    }
    // seteo las coordenadas obtenidas al itinerario para después usarlas
    // para obtener los places, hoteles, etc
    itinerary.setCoordenadas(paramsResponse);
  
    return itinerary.getHotels(cantidadDiasIt);

  }).then((result)=>{
    // cada vez que hago un get, devuelvo un itinearario y voy pidiendo los getters que me quedan
    return result.getFoodPlaces(cantidadDiasIt);
  }).then((result)=>{
    return result.getExcusrionPlaces(cantidadDiasIt);
  }).then((result)=>{

    // guardo todas las promesas juntas para que cuando estén todas resueltas me cree el itinerario
    const promises=[itinerary.foodPlaces,itinerary.excursionPlaces,itinerary.hotel];
    Promise.all(promises).then((result)=>{
      res.json(mkItinear(result));
    });
    
  }).catch((error) => {
    console.log(error);
    res.json(mk_error_response(error)); 
  });
}

function createItinerary(setPlaces){
  //console.log(setPlaces);
  let result=[];
  let saveSetPlaces=setPlaces;
  var i=0;
  for(; i<setPlaces[2].length*2; i+=2){
    let day= new Day(saveSetPlaces[0].slice(i,i+2),
                     saveSetPlaces[1].slice(i,i+2),
                     saveSetPlaces[2].slice(i,i+1));
    
  saveSetPlaces[0].splice(i,i+2);
  saveSetPlaces[1].slice(i,i+2);
  saveSetPlaces[2].slice(i,i+1);

    console.log("day -------------------------------");
    console.log(day);
    result.concat(day);
  }
  console.log(result);
  return result;
}

function buscar(coordenadas, cantQueNecesito, total, filtro) {
 /**
  * Buscar es una funcion recurisva. Si no encuentra la cantidad de lugares que necesito,
  * llama a la función buscar pero con las 
  */
  

  return client.nearbySearch(coordenadas).then(
    (response) => {
      let results = response.body.results;
      // acá filtramos con la función pasado como parámetro que es filtro
      let newTotal = total.concat(filtro(results)); 

      if(newTotal.length >= cantQueNecesito) {
        return newTotal.splice(0,cantQueNecesito);
      }
      else {
        // con la funcion 
        let coordenadasModificadas = modificadorCoordenadas(coordenadas);
        return buscar(coordenadasModificadas, cantQueNecesito, newTotal, filtro);
      }
  }).catch((error)=>{
    console.log(error);
  });
}

let count=1
let move =0.30;

function modificadorCoordenadas(paramsResponse){
  /**
   * Va modificando las coordenadas yendo al norte, este,sur y oeste con la función desplazarLocation
   * Si ya hizo el llamado de norte a oeste, se vuelve a llamr la función pero desplazando las coordenadas
   * con la variable move
   */ 
  if(count<= 4){
    let newLocation =  desplazarLocation(paramsResponse);
    return newLocation;
    count+=1;
  }
  else{
    count= 1;
    move+= 0.30;
    modificadorCoordenadas(paramsResponse)
  }
}
// funciones para sumar y restar latitudes y longitud
function sumarLatitud(paramsReponse,desplazamiento){
  let latitudFloat = parseFloat(getLatitud(paramsReponse.location));
  let latitudString = (latitudFloat + desplazamiento).toString();
  return latitudString;
}

function sumarLongitud(paramsReponse,desplazamiento){
  let latitudFloat = parseFloat(getLongitud(paramsReponse));
  let latitudString = (latitudFloat + desplazamiento).toString();
  return latitudString;
}

function restarLatitud(paramsReponse,desplazamiento){
  let latitudFloat = parseFloat(getLatitud(paramsReponse));
  let latitudString = (latitudFloat - desplazamiento).toString();
  return latitudString;
}

function restarLongitud(paramsReponse,desplazamiento){
  let latitudFloat = parseFloat(getLongitud(paramsReponse));
  let latitudString = (latitudFloat - desplazamiento).toString();
  return latitudString;
}
// getter latitud
function getLatitud(params){
  let latitud = params.location.substring(0, params.location.indexOf(","));
  return latitud;
}
// getter longitud
function getLongitud(params){
  let indexStart = params.location.indexOf(",") + 1;
  let indexEnd = params.location.length;
  let longitud = params.location.substring(indexStart, indexEnd);
  return longitud;
}


function desplazarLocation(paramsReponse){
/** se encarga de mover las coordenadas usando la variable count
 *  Si count
 *    es 1 -> desplaza al norte
 *    es 2 -> desplaza al este
 *    es 3 -> desplaza al oeste
 *    es 4 -> desplaza al sur
 */
  let newParamsRespose;

  switch(count){
    case 1:
      newParamsRespose= getLatitud(paramsReponse) + "," + sumarLongitud(paramsReponse, move);
      break;
    case 2:
      newParamsRespose= restarLongitud(newParamsRespose, move);
      newParamsRespose= sumarLatitud(newParamsRespose, move) + "," + getLongitud(newParamsRespose.location);
      break;
    case 3:
      newParamsRespose= restarLatitud(newParamsRespose, move);
      newParamsRespose= getLatitud(newParamsRespose.location) + "," + restarLongitud(newParamsRespose, move);
      break;
    case 4:
      newParamsRespose= restarLongitud(newParamsRespose, move);
      newParamsRespose= restarLatitud(newParamsRespose, move) + "," + getLongitud(newParamsRespose.location);
      break;
  }
  let object = {
    location: newParamsRespose,
    radius: paramsReponse.radius
  };
  return object;
}
// filtros de foodPlaces, excursionPlaces y hoteles
function filterFoodPlaces(listPlaces) {
  return listPlaces.filter(p => p.types.includes('food') ||
                                            p.types.includes('bar') ||
                                            p.types.includes('cafe') ||
                                            p.types.includes('bakery') ||
                                            p.types.includes('restaurant'));
}

function filterExcursionPlaces(listPlaces) {
  return listPlaces.filter(p => p.types.includes('point_of_interest') || 
                                                 p.types.includes('aquarium') || 
                                                 p.types.includes('art_gallery') ||
                                                 p.types.includes('movie_theater') ||
                                                 p.types.includes('natural_feature'));
}

function filterHotel(listPlaces) {
  return listPlaces.filter(p => p.types.includes('lodging'))
}

class Day {
    
  constructor(twoFoodPlaces, twoExcursionsPlaces, hotel) {
    this.paseoManiana = datosLugar(twoExcursionsPlaces[0]);
    this.almuerzo = datosLugar(twoFoodPlaces[0]);
    this.paseTarde =datosLugar(twoExcursionsPlaces[1]);
    this.cena = datosLugar(twoFoodPlaces[1]);
    this.hotel = datosLugar(hotel[0]);
  }
}

function datosLugar(place){
  // devuelve el objeto que nos pide el enunciado 
  return{
    name:place.name,
    adress:place.vicinity,
    rating:place.rating
  }
}

class Itinerary {
  
  constructor() {
    this.coordenadas = null;
    this.foodPlaces = null;
    this.hotel = null;
    this.excursionPlaces = null;
  }

  setCoordenadas(unasCoordenadas){
    this.coordenadas=unasCoordenadas;
  }

  getHotels(cantDays){
    this.hotel= buscar(this.coordenadas,cantDays,[],filterHotel);
    return this;
  }

  getFoodPlaces(cantDays){
    this.foodPlaces= buscar(this.coordenadas,cantDays*2,[],filterFoodPlaces);
    return this;
  }

  getExcusrionPlaces(cantDays){
    this.excursionPlaces= buscar(this.coordenadas,cantDays*2,[],filterExcursionPlaces);
    return this;
  }
}

module.exports = {
  filterExcursionPlaces,
  filterFoodPlaces,
  filterHotel,
  places
}